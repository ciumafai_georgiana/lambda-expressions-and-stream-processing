import java.time.Duration;
import java.time.LocalDateTime;

public class MonitoredData {
    private String activity;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    public MonitoredData() {
    }

    public MonitoredData(String activity, LocalDateTime startTime, LocalDateTime endTime) {
        this.activity = activity;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }



    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "MonitoredData{" +
                "activity='" + activity + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
    public Long getTimeBetween(){
        Duration d=Duration.between(startTime,endTime);
        long seconds=d.getSeconds();
        return seconds;
    }
}
