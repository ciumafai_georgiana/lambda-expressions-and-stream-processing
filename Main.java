import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.time.temporal.ChronoUnit;

import static java.time.temporal.ChronoUnit.DAYS;

public class Main {

    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
        String fileName = "E:\\AN 3\\tema5\\src\\Activities.txt";
        List<MonitoredData> data = new ArrayList<MonitoredData>();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(t -> {
                String[] s = t.split("\t\t");
                        LocalDateTime startTime = LocalDateTime.parse(s[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                        LocalDateTime endTime = LocalDateTime.parse(s[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                        MonitoredData monitoredData = new MonitoredData(s[2], startTime, endTime);
                        data.add(monitoredData);
                    }
            );
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        for (MonitoredData m : data) {
            System.out.println(m.toString());
        }
        //TASK 2
        //Count how many days of monitored data appears in the log.
        long daysOfActivities = data.stream().map((t -> t.getStartTime().getDayOfYear())).distinct().count();
        System.out.println(daysOfActivities);
        //TASK 3
        //Count how many times has appeared each activity over the entire monitoring period.
        //Return a map of type <String, Int> representing the mapping of activities to their count.
        Map<String, Long> activities = data.stream().collect(Collectors.groupingBy(c -> c.getActivity(), Collectors.counting()));
        PrintWriter printWriter;
        try {
            printWriter = new PrintWriter("task 3.txt", "UTF-8");
            for (Map.Entry<String, Long> entry : activities.entrySet()) {
                printWriter.println(entry.getKey() + " " + entry.getValue());
            }
            printWriter.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //TASK 4
        //Count how many times has appeared each activity for each day over the monitoring period
        Map<Integer, Map<String, Long>> mapMap;
        mapMap =data.stream().collect(Collectors.groupingBy(a->a.getStartTime().getDayOfMonth(),Collectors.groupingBy(a->a.getActivity(),Collectors.counting())));
        PrintWriter printWriter1;
        try{
            printWriter1=new PrintWriter("task 4.txt","UTF-8");
            for(Map.Entry<Integer,Map<String,Long>> entry:mapMap.entrySet()){
                for(Map.Entry<String,Long> entry1:entry.getValue().entrySet()){
                    printWriter1.println("Day "+entry.getKey() + " Activity " +entry1.getKey() + " occurred " + entry1.getValue() + " time ");
                }
            }
            printWriter1.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //TASK 5
        //For each line from the file map for the activity label the duration recorded on that line
        //(end_time-start_time)
        PrintWriter printWriter3=new PrintWriter("task 5.txt","UTF-8");
        //List<MonitoredData> data2 = new ArrayList<MonitoredData>();
        try {
            data.forEach(t -> {
                        t.getTimeBetween();
                        Long d = t.getTimeBetween();
                        printWriter3.println("Activity " + t.getActivity() + " lasted " + d+ " seconds ");
                        // data2.add(monitoredData2);
                    }
            );
            printWriter3.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //TASK 6
        //For each activity compute the entire duration over the monitoring period
        Map<String,Long> dur=data.stream().collect(Collectors.groupingBy(a->a.getActivity(),Collectors.summingLong(a->a.getTimeBetween())));
        PrintWriter printWriter4;
        try {
            printWriter4 = new PrintWriter("task 6.txt", "UTF-8");
            for (Map.Entry<String, Long> entry : dur.entrySet()) {
                printWriter4.println(" Activity " + entry.getKey() + " lasted for " + entry.getValue() + " seconds");
            }
            printWriter4.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }




    }
}